#!/usr/bin/env node

import { Command } from 'commander';
import { createRequire } from 'node:module';

const require = createRequire(import.meta.url);
const { description, version } = require('../package.json');

const program = new Command();

program
  .name('gitee-release')
  .description(description)
  .version(version)
  .command('create', 'create a release')
  .command('config', 'operate configuration')
  .command('assets', 'manage the assets of release')
  .parse(process.argv);
