import { Command } from "commander";
import { uploadAsset } from "../src/index.js";

const program = new Command();

program.name("gitee-release-assets");

program
  .command("upload")
  .argument("<file>")
  .option("--target <version>", "specify target release by version")
  .action((file, options) => uploadAsset(file, options.target));

program.parse(process.argv);
