#!/usr/bin/env node

import { Command } from 'commander';
import { config } from '../src/index.js';
import logger from '../src/logger.js';

const program = new Command();

program
  .description('Operate configuration')
  .argument('<name>', 'name of the configuration')
  .argument('<value>', 'value of the configuration')
  .action((name, value) => {
    if (typeof value !== 'string') {
      logger.log(config.get(name) || '');
    } else {
      config.set(name, value || '');
    }
  })
  .parse(process.argv);
