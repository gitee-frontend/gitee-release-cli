#!/usr/bin/env node

import { Command } from "commander";
import prompts from "prompts";
import { execSync } from "child_process";
import logger from "../src/logger.js";
import { collect, getNextVersion, generate, upload } from "../src/index.js";

const program = new Command();

program
  .option(
    "--with-npm-version",
    "run `npm version` to update the version to package.json"
  )
  .option(
    "--with-git-push",
    "run `git push` to push commits and tags to remote"
  )
  .option(
    "--prerelease <identifiers>",
    "add prerelease tag, identifiers can be alpha or beta",
    false
  )
  .option("--upload", "upload the release to Gitee.com without prompting")
  .option(
    "--latest-version <version>",
    "specify the latest version and let the next version start incrementing from it."
  )
  .option("--no-version-selection", "no prompting for version selection")
  .action(async (options) => {
    let response;
    const data = await collect(options.latestVersion);
    let version = getNextVersion({ ...data, prerelease: options.prerelease });

    logger.log(
      `current version contains ${data.changes.breakingChanges} breaking changes,` +
        ` ${data.changes.features} features, ${data.changes.bugs} bug fixes.`
    );
    logger.log(`version will update from ${data.latestVersion} to ${version}`);

    if (options.versionSelection) {
      response = await prompts([
        {
          name: "selectVersion",
          type: "select",
          message: `Do you want to set the new version to ${version}?`,
          choices: [
            { title: "Yes", description: "Use this version", value: "yes" },
            { title: "No", description: "Use custom version", value: "no" },
            { title: "Cancel", value: "cancel" },
          ],
        },
        {
          type: (prev) => (prev === "no" ? "text" : null),
          name: "version",
          message: "Please input version",
        },
      ]);
      if (response.selectVersion === "cancel") {
        return;
      }
      if (response.selectVersion === "no") {
        version = response.version;
      }
    }

    const release = generate({
      ...data,
      version,
      prerelease: options.prerelease,
      latestVersion: options.latestVersion,
    });

    if (options.withNpmVersion) {
      logger.log("update the version to package.json");
      execSync(
        `npm version ${release.tag_name} -m "chore(release): ${release.tag_name}"`,
        { stdio: "inherit" }
      );
    }
    if (options.withGitPush) {
      logger.log("push commits and tags to remote");
      execSync("git push origin master --tags", { stdio: "inherit" });
    }
    if (!options.upload) {
      logger.log(release);
      response = await prompts({
        name: "upload",
        type: "confirm",
        message: "Do you want to upload the above?",
      });
      if (response.upload) {
        upload(release);
      }
    } else {
      upload(release);
    }
  })
  .parse(process.argv);
