# gitee-release-cli

[![npm](https://img.shields.io/npm/l/gitee-release-cli.svg)](https://gitee.com/gitee-frontend/gitee-release-cli/blob/master/LICENSE)
[![npm](https://img.shields.io/npm/v/gitee-release-cli.svg)](https://gitee.com/gitee-frontend/gitee-release-cli/releases)
[![gitee-release-cli](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-gitee--release-blueviolet.svg)](https://gitee.com/gitee-frontend/gitee-release-cli)

## 介绍

一个用于创建发行版的命令行工具。

## 特性

- 强制执行[语义化版本](https://semver.org)规范
- 基于 [Gitee API](https://gitee.com/api/v5/swagger#/) 为你的项目创建发行版
- 采用 [commitlint](https://github.com/conventional-changelog/commitlint) 的提交信息规范中的约定来记录代码库中的改动

## 安装

``` bash
npm install -g gitee-release-cli
```

## 使用

先前往[私人令牌页](https://gitee.com/profile/personal_access_tokens)页创建一个令牌，然后配置：

```bash
gitee-release config accessToken 你的令牌
```

设置使用中文版的内容（只翻译标题）：

```bash
gitee-release config locale zh_CN
```

创建一个发行版：

```bash
# 创建发行版，然后提示是否上传发行版到码云
gitee-release create

# 创建一个 beta 预发行版
gitee-release create --prerelease beta

# 生成版本号后运行 npm version 更新版本号，然后运行 git push 推送改动内容
gitee-release create --with-npm-version --with-git-push --upload
```

给最新的发行版上传一个附件：

```bash
gitee-release assets upload /path/to/file.zip
```

给指定版本（例如：2.3.0）的发行版上传一个附件：

```bash
gitee-release assets upload /path/to/file.zip --target 2.3.0
```


如果觉得手动输入这些命令太麻烦，可在 package.json 中添加以下内容：

```json
{
    "scripts": {
        "release": "gitee-release create --with-npm-version --with-git-push --upload",
        "release-beta": "npm run release -- --prerelease beta",
    }
}
```

## 小徽标

将此徽标包含在 README.md 自述文件中，可以让他人知道你的项目是使用 gitee-release-cli 发布的。

[![gitee-release-cli](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-gitee--release-blueviolet.svg)](https://gitee.com/gitee-frontend/gitee-release-cli)

```markdown
[![gitee-release-cli](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-gitee--release-blueviolet.svg)](https://gitee.com/gitee-frontend/gitee-release-cli)
```

## 常见问题

- **我为什么要选择 gitee-release-cli 而不是 semantic-release？**

    如果你懒得看 [semantic-release](https://github.com/semantic-release/semantic-release) 的英文说明文档的话，可以先试试 gitee-release-cli。

- **对代码库里的提交信息格式有什么要求？**

    需要遵循 [commitlint](https://github.com/conventional-changelog/commitlint) 的提交信息规范。

## 许可

基于 [MIT 许可协议](LICENSE)发布。
