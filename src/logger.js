import chalk from 'chalk';

const logger = {
  log(...args) {
    console.log(...args);
  },
  error(...args) {
    console.error(chalk.bold.red('error:'), ...args);
  },
  warning(...args) {
    console.error(chalk.yellow('warning:'), ...args);
  },
};

export default logger;
